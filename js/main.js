
/**
 * Main AngularJS Web Application
 */
var app = angular.module('konnectoApp', ['ngRoute', 'ngResource']);

/**
 * Configure the Routes
 */
app.config(['$routeProvider', function ($routeProvider) {
  $routeProvider
    // Home
    .when("/", {templateUrl: "partials/home.html", controller: "mainCtrl"})
    // else 404
    .otherwise('404');
}]);

/**
 * Controls the Blog
 */
app.controller('BlogCtrl', function (/* $scope, $location, $http */) {
  console.log("Blog Controller reporting for duty.");
});
