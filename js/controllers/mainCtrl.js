var app = angular.module('konnectoApp');

app.controller('mainCtrl', function ($scope, Login, Setters) {

  $scope.user= null;
  $scope.social = null;
  var init = function(){

  };

  $scope.login = function(socialSite) {
    $scope.social = socialSite;
    // activate spinner
    $scope.loading = true;
    var response = Login[socialSite + 'Obj'];
    getUserComplete(response);
  };

  var getUserComplete = function(response){
    $scope.success = response.success;
    if(response.success){
      var callBackFuncName = $scope.social + 'ObjSetter'
      // set the user object
      $scope.user = Setters[callBackFuncName](response, {social: $scope.social});
    }else{
      $scope.user ={};
    }
    //deactivate spinner
    $scope.loading = false;
  };
/*
   $scope.login = function(socialSite){
       $scope.social = socialSite;
       $scope.loading = true;

        Login.api.signin({act:socialSite},{	user_id:"123"}, getUserComplete, function(err){
          console.error(err);
          $scope.loading = false;
          $scope.success = false;
          $scope.social = null;
          $scope.user ={};
        });
   }
*/
   init();

});
