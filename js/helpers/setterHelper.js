app.factory('Setters', ['$resource', function($resource) {
  /** structure
  social: //String,
  fName: //String,
  lName: //String,
  msg: //String
  pic: //String link,
  **/
  var userObj = {}
  var User = function(social, fName, lName, msg, pic) {
    userObj = {
      social: social,
      fName: fName,
      lName: lName,
      msg: msg,
      pic: pic
    };
  };



 return {
   googleObjSetter : function(userRes, moreOpions){
     User(moreOpions.social, userRes.user.first_name, userRes.user.last_name, userRes.msg, userRes.user.pic);
     return userObj;
   },
   instagramObjSetter : function(userRes, moreOpions){
     //var name = userRes.user.name.split(" ");
     User(moreOpions.social, userRes.user.name, null, userRes.msg, userRes.user.pic);
     return userObj;
   },
   twitterObjSetter : function(userRes, moreOpions){
     User(moreOpions.social, userRes.data.user.uname, null, userRes.msg, userRes.data.user.pic);
     return userObj;
   },
   amazonObjSetter : function(userRes, moreOpions){
     User(moreOpions.social, userRes.user.first_name, userRes.user.last_name, userRes.msg, userRes.user.pic);
     return userObj;
   },
   fitbitObjSetter : function(userRes, moreOpions){
     User(moreOpions.social, userRes.user.first_name, userRes.user.last_name, userRes.msg, userRes.user.pic);
     return userObj;
   }
 };

}]);
