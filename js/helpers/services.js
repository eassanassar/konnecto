var app = angular.module('konnectoApp');

app.factory('Login', ['$resource', function($resource) {
  // :act is the social media name
var googleObj = {
    "success": true,
    "msg": "Welcome Google User",
    "user": {
        "first_name": "Blinky",
        "last_name": "Bill",
        "pic": "http://vignette2.wikia.nocookie.net/poohadventures/images/4/42/Blinky_Bill.jpg/revision/latest?cb=20140401174128"
    }
};
var instagramObj = {
    "success": true,
    "msg": "Welcome Instagram User",
    "user": {
        "name": "Blinky Bill",
        "user_name": "The king",
        "pic": "https://www.heyuguys.com/images/2016/08/maxresdefault-1.jpg"
    }
};
var twitterObj = {
    "success": true,
    "msg": "Welcome Twitter User",
    "data": {
        "user": {
            "uname": "BB",
            "age": 16,
            "pic": "https://res.cloudinary.com/beamly/image/upload/s--92G9WERa--/c_fill,g_face,q_70,w_479/f_jpg/v1/click/sites/8/2014/11/blinky_90.jpg"
        }
    }
};
var amazonObj = {
    "success": true,
    "msg": "Welcome Amazon User",
    "user": {
        "first_name": "Blinky",
        "last_name": "Bill",
        "prime_member": true,
        "pic": "https://static1.squarespace.com/static/54fd2e4de4b085f9b7aeca3b/t/5844c144725e25d0d2b85aab/1480900943608/Blinky_Profile.jpg"
    }
};
var fitbitObj = {
    "success": true,
    "msg": "Welcome Fitbit User",
    "user": {
        "first_name": "Blinky",
        "last_name": "Bill",
        "pic": "https://pbs.twimg.com/profile_images/581265647096623104/KZXfJaPa.jpg"
    }
};

 return {
   api: $resource('https://eassa.konnecto.io/api/user/:act/login', null, {
    'signin': { method: 'POST' }
    }),
    googleObj:googleObj,
    instagramObj:instagramObj,
    twitterObj:twitterObj,
    amazonObj:amazonObj,
    fitbitObj:fitbitObj
  }
}]);
